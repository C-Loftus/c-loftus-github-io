---
showReadingTime: false
showTOC: false
title: My background
summary: About me.
categories:
  - Contact
---



Hello, I’m Colton. I’m currently a senior at Princeton University studying computer science, with a minor in technology policy. I am specialized in machine learning.

I am currently interested in a career where I can combine my quantitative skills in tech with my interests in business and finance. In my spare time I am interested in environmental work and sustainability.

I have a well-rounded business background with a variety of expertise in data science, consulting, machine learning, web development, blockchain development, and disability software.

Thank you for viewing my website and please feel free to reach out to me at the email listed in my Resume or any website in the contacts page.

## My Resume

(View on desktop for proper formatting)
{{< rawhtml >}}

<style>
.iframe-container {
  overflow: hidden;
  /* 16:9 aspect ratio */
  padding-top: 156.25%;
  padding-left: 10000%;
  position: relative;
}
.iframe-container iframe {
   border: 0;
   height: 100%;
   left: 0;
   position: absolute;
   top: 0;
   width: 100%;
}
</style>
<div class="iframe-container">
<iframe src="https://docs.google.com/document/d/e/2PACX-1vTmtzuI61yZgFChsUmDTbuqi0QrrvUywwp-BnJx5UdiifO7IEyWzFrmU_pfySICbS15CA80Ji0_HpG-/pub?embedded=true"></iframe>
</div>
{{< /rawhtml >}}
