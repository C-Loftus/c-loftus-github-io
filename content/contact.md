---
showReadingTime: false
showTOC: false
title: Contact
summary: Let's Connect!
categories:
  - Contact
---

### Email

To prevent spam, my school and personal emails can be found in my resume on the [about tab](/about/).

### LinkedIn

https://www.linkedin.com/in/coltonl/

### Github

https://github.com/C-Loftus

### Matrix

I can be reached on Matrix at @colton:matrix.colton.world ; My homeserver federates with all others, but send an email if you wish to be added to my homeserver specifically.

<!--
### Crypto

My BTC address is 0xbc1qh439p4w6zn3djuskx3d7g7mvhstyp94zjvna8rz -->

### Meetings

To set up a meeting, I have a jitsi video call service located at : https://meet.colton.world/
